# build-variants_app

A demo of the besst practices of keeping api keys out of the git repoo while at the same time being able to do endpoints and other environment vars for testing.

## Theory

Flutter is a front end app framework. Both Android and iOS have native hooks via their native build tools to assist in keeping api keys out of the git repo.

But, with flutter we do not have access to those native build tools to customize. Instead I use meta-interfaces as contracts and some fancy gitignoring to make it fool proof to keep 
api keys out of the git repo.

# Credits

# License

BSD Clause 2


# Resources

This is part of my FlutterPatterns collection at

[FlutterPatterns](https://gitlab.com/fred.grott/flutterpatterns)